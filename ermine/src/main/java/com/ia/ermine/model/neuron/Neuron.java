package com.ia.ermine.model.neuron;

import com.ia.ermine.model.activation_function.ActivationFunction;

public class Neuron {
	
	/**
	 * activation function that gives life to the neuron
	 */
	private ActivationFunction activationFunction;

	/**
	 * Accesseur en lecture de activationFunction
	 * @return the activationFunction
	 */
	public ActivationFunction getActivationFunction() {
		return activationFunction;
	}

	/**
	 * Accesseur en écriture de activationFunction
	 * @param activationFunction the activationFunction to set
	 */
	public void setActivationFunction(ActivationFunction activationFunction) {
		this.activationFunction = activationFunction;
	}
	
	

}
