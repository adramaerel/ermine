package com.ia.ermine.model.activation_function;

public class HardLimitingThreshold extends ActivationFunction {

	@Override
	public Double calculus(Double inputWeight) {
		// TODO Auto-generated method stub
		return new Double(inputWeight >= 0 ? 1 : 0);
	}

}
