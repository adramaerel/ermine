package com.ia.ermine.model.activation_function;

/**
 * Modélise le calcul fait par le neurone. L'output est calculé en fonction des input reçus par 
 * le neurone et de l'activationFunction.
 * @author Sophie Moreira
 *
 */
public abstract class ActivationFunction {
	
	/**
	 * the calculus itself.
	 * @param inputWeight weight in input
	 * @return the ouput
	 */
	public abstract Double calculus(Double inputWeight);
	
	

}
