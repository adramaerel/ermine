package com.ia.ermine.model.activation_function;

public class Sigmoid extends ActivationFunction {

	@Override
	public Double calculus(Double x) {
		
		return 1 / (1 + Math.exp(-x));
	}

}
