package com.ia.ermine.model.activation_function;

public class HyperbolicTangent extends ActivationFunction {

	@Override
	public Double calculus(Double x) {
		
		return (1 - Math.exp(-x))/(1 + Math.exp(-x));
	}

}
